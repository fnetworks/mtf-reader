#![feature(const_int_conversion)]

use std::fs::File;

pub mod mtf;
use mtf::{
    align_stream, BlockType, DescriptorBlock, DescriptorBlockData, MediaLabel, StreamHeader,
    StreamId,
};

use std::io::{Seek, SeekFrom, Read};

use tar::{Builder, Header};

fn main() {
    let mut input = File::open(
        "/media/fnet/ExpansionDrive 2/Users/Documents/Andere/sicherung_festplatte_alt_comp.bkf",
    )
    .unwrap();

    let output = File::create("output.tar").unwrap();
    let mut tar_builder = Builder::new(output);

    let mut current_volume: Option<String> = None;
    let mut current_file: Option<String> = None;
    let mut current_directory: Option<String> = None;

    // let mut i = 0u32;
    loop {
        // i += 1;
        let block = DescriptorBlock::read(&mut input).unwrap();

        // println!(
        //     "{:?} (@{})",
        //     block.common_block_header.descriptor_block_type, block.base_address
        // );
        if let DescriptorBlockData::TapeHeader(ref block_data) = block.block_data {
            if let Some(name) = block.read_string_at(&mut input, block_data.media_name) {
                println!("\tName: {}", name.unwrap());
            }
            if let Some(description_label) =
                block.read_string_at(&mut input, block_data.media_description_label)
            {
                let label = MediaLabel::from_string(&description_label.unwrap());
                println!("\tDescription:");
                println!("\t\tTag: {}", label.tag);
                println!("\t\tVersion: {}", label.version);
                println!("\t\tVendor Product ID: {}", label.vendor_product_id);
                println!("\t\tCartridge Label: {}", label.cartridge_label);
            }
        } else if let DescriptorBlockData::File(ref block_data) = block.block_data {
            let name = block.read_string_at(&mut input, block_data.file_name).unwrap().unwrap();
            // println!("\tName: {}", name);
            current_file = Some(name);
        } else if let DescriptorBlockData::Directory(ref block_data) = block.block_data {
            let name = block.read_string_at(&mut input, block_data.directory_name).unwrap().unwrap();
            let name = name.split('\u{0}').filter(|e| e.len() > 0).collect::<Vec<_>>().join("/");
            // println!("\tName: {}", name);
            current_directory = Some(name);
        } else if let DescriptorBlockData::Volume(ref block_data) = block.block_data {
            let name = block.read_string_at(&mut input, block_data.device_name).unwrap().unwrap();
            // println!("\tName: {}", name);
            current_volume = Some(name);
        }

        input
            .seek(SeekFrom::Start(
                block.base_address + (block.common_block_header.offset_to_first_event as u64),
            ))
            .unwrap();
        if block.common_block_header.descriptor_block_type != BlockType::SoftFileMark {
            loop {
                let hdr = StreamHeader::read(&mut input).unwrap();
                // println!("> {:?}", hdr.stream_id);
                if hdr.stream_id == StreamId::Pad {
                    input
                        .seek(SeekFrom::Current(hdr.stream_length as i64))
                        .unwrap();
                    break;
                } else if hdr.stream_id == StreamId::Standard {
                    if let DescriptorBlockData::File(ref block_data) = block.block_data {
                        let current_file: String = current_file.clone().unwrap();
                        let current_directory: String = current_directory.clone().unwrap();
                        let pth = {
                            if current_directory.is_empty() {
                                format!("{}", current_file)
                            } else {
                                format!("{}/{}", current_directory, current_file)
                            }
                        };

                        let mut header = Header::new_gnu();
                        if let Ok(_) = header.set_path(pth) {
                            header.set_size(hdr.stream_length);
                            header.set_mode(0o644);
                            header.set_cksum();
                            let mut buf = vec![0u8; hdr.stream_length as usize];
                            input.read_exact(&mut buf).unwrap();
                            tar_builder.append(&header, buf.as_slice()).unwrap();
                        }
                    }
                } else {
                    input
                        .seek(SeekFrom::Current(hdr.stream_length as i64))
                        .unwrap();
                }
                align_stream(&mut input, 4).unwrap();
            }
        }

        // if i == 7 {
        //     break;
        // }

        if block.common_block_header.descriptor_block_type == BlockType::EndOfTapeMarker {
            break;
        }
    }

    tar_builder.finish().unwrap();
}
