use super::read::MtfReadExt;
use num_enum::{IntoPrimitive, TryFromPrimitive};
use std::convert::TryFrom;
use std::io::{self, Read};
use crate::str2u32;

const STANDARD_DATA_STREAM: u32 = str2u32!(b"STAN");
const PATH_NAME_STREAM: u32 = str2u32!(b"PNAM");
const FILE_NAME_STREAM: u32 = str2u32!(b"FNAM");
const CHECKSUM_STREAM: u32 = str2u32!(b"CSUM");
const CORRUPT_STREAM: u32 = str2u32!(b"CRPT");
const PAD_STREAM: u32 = str2u32!(b"SPAD");
const SPARSE_STREAM: u32 = str2u32!(b"SPAR");
const MBC_LMO_SET_MAP_STREAM: u32 = str2u32!(b"TSMP");
const MBC_LMO_FDD_STREAM: u32 = str2u32!(b"TFDD");
const MBC_SLO_SET_MAP_STREAM: u32 = str2u32!(b"MAP2");
const MBC_SLO_FDD_STREAM: u32 = str2u32!(b"FDD2");

const NTFS_ALT_STREAM: u32 = str2u32!(b"ADAT");
const NTFS_EA_STREAM: u32 = str2u32!(b"NTEA");
const NT_SECURITY_STREAM: u32 = str2u32!(b"NACL");
const NT_ENCRYPTED_STREAM: u32 = str2u32!(b"NTED");
const NT_QUOTA_STREAM: u32 = str2u32!(b"NTQU");
const NT_PROPERTY_STREAM: u32 = str2u32!(b"NTPR");
const NT_REPARSE_STREAM: u32 = str2u32!(b"NTRP");
const NT_OBJECT_ID_STREAM: u32 = str2u32!(b"NTOI");

#[derive(Debug, Copy, Clone, PartialEq, Eq, TryFromPrimitive, IntoPrimitive)]
#[repr(u32)]
pub enum StreamId {
    Unknown = 0,
    Standard = STANDARD_DATA_STREAM,
    PathName = PATH_NAME_STREAM,
    FileName = FILE_NAME_STREAM,
    Checksum = CHECKSUM_STREAM,
    Corrupt = CORRUPT_STREAM,
    Pad = PAD_STREAM,
    Sparse = SPARSE_STREAM,
    MbcLmoSetMap = MBC_LMO_SET_MAP_STREAM,
    MbcLmoFdd = MBC_LMO_FDD_STREAM,
    MbcSloSetMap = MBC_SLO_SET_MAP_STREAM,
    MbcSloFdd = MBC_SLO_FDD_STREAM,

    // NT Data
    NtfsAlt = NTFS_ALT_STREAM,
    NtfsEa = NTFS_EA_STREAM,
    NtSecurity = NT_SECURITY_STREAM,
    NtEncrypted = NT_ENCRYPTED_STREAM,
    NtQuota = NT_QUOTA_STREAM,
    NtProperty = NT_PROPERTY_STREAM,
    NtReparse = NT_REPARSE_STREAM,
    NtObjectId = NT_OBJECT_ID_STREAM,
}

#[derive(Debug, Copy, Clone)]
pub struct StreamHeader {
    pub stream_id: StreamId,
    pub stream_file_system_attributes: u16,
    pub stream_media_format_attributes: u16,
    pub stream_length: u64,
    pub data_encryption_algorithm: u16,
    pub data_compresssion_algorithm: u16,
    pub checksum: u16,
}

impl StreamHeader {
    pub fn read<T>(read: &mut T) -> io::Result<Self>
    where
        T: Read,
    {
        let raw_stream_id = read.read_u32()?;
        let stream_id = StreamId::try_from(raw_stream_id).unwrap_or(StreamId::Unknown);
        if stream_id == StreamId::Unknown {
            let mut sid_as_hex = [b' '; 4];
            sid_as_hex[0] = (raw_stream_id & 0xFF) as u8;
            sid_as_hex[1] = ((raw_stream_id >> 8) & 0xFF) as u8;
            sid_as_hex[2] = ((raw_stream_id >> 16) & 0xFF) as u8;
            sid_as_hex[3] = ((raw_stream_id >> 24) & 0xFF) as u8;
            println!(
                "Unknown StreamId: {:x} ({})",
                raw_stream_id,
                std::str::from_utf8(&sid_as_hex).unwrap()
            );
        }
        let stream_file_system_attributes = read.read_u16()?;
        let stream_media_format_attributes = read.read_u16()?;
        let stream_length = read.read_u64()?;
        let data_encryption_algorithm = read.read_u16()?;
        let data_compresssion_algorithm = read.read_u16()?;
        let checksum = read.read_u16()?;
        Ok(Self {
            stream_id,
            stream_file_system_attributes,
            stream_media_format_attributes,
            stream_length,
            data_encryption_algorithm,
            data_compresssion_algorithm,
            checksum,
        })
    }
}
