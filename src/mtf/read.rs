use std::convert::TryInto;
use std::io::{Read, Result, Seek, SeekFrom};

pub trait MtfReadExt: Read {
    fn read_u8(&mut self) -> Result<u8> {
        let mut buf = [0; 1];
        self.read_exact(&mut buf)?;
        Ok(u8::from_le_bytes(buf))
    }

    fn read_i8(&mut self) -> Result<i8> {
        let mut buf = [0; 1];
        self.read_exact(&mut buf)?;
        Ok(i8::from_le_bytes(buf))
    }

    fn read_u16(&mut self) -> Result<u16> {
        let mut buf = [0; 2];
        self.read_exact(&mut buf)?;
        Ok(u16::from_le_bytes(buf))
    }

    fn read_i16(&mut self) -> Result<i16> {
        let mut buf = [0; 2];
        self.read_exact(&mut buf)?;
        Ok(i16::from_le_bytes(buf))
    }

    fn read_u32(&mut self) -> Result<u32> {
        let mut buf = [0; 4];
        self.read_exact(&mut buf)?;
        Ok(u32::from_le_bytes(buf))
    }

    fn read_i32(&mut self) -> Result<i32> {
        let mut buf = [0; 4];
        self.read_exact(&mut buf)?;
        Ok(i32::from_le_bytes(buf))
    }

    fn read_u64(&mut self) -> Result<u64> {
        let lsb: u64 = self.read_u32()?.into();
        let msb: u64 = self.read_u32()?.into();
        Ok((msb << 32) | lsb)
    }
}

impl<R: Read + ?Sized> MtfReadExt for R {}

pub fn align_stream<T>(stream: &mut T, align: u64) -> Result<()>
where
    T: Seek,
{
    if align == 0 {
        return Ok(());
    }

    let current_pos = stream.seek(SeekFrom::Current(0))?;
    let remaining = align - (current_pos % align);
    if remaining > 0 {
        stream.seek(SeekFrom::Current(remaining.try_into().unwrap()))?;
    }
    Ok(())
}
