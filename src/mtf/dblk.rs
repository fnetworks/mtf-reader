use super::common::{DateTime, TapeAddress};
use super::read::MtfReadExt;
use encoding_rs::{UTF_16LE, WINDOWS_1252};
use num_enum::{IntoPrimitive, TryFromPrimitive};
use std::convert::{TryFrom, TryInto};
use std::io::{self, Read, Seek, SeekFrom};
use crate::str2u32;

const MTF_TAPE: u32 = str2u32!(b"TAPE");
const MTF_SSET: u32 = str2u32!(b"SSET");
const MTF_VOLB: u32 = str2u32!(b"VOLB");
const MTF_DIRB: u32 = str2u32!(b"DIRB");
const MTF_FILE: u32 = str2u32!(b"FILE");
const MTF_CFIL: u32 = str2u32!(b"CFIL");
const MTF_ESPB: u32 = str2u32!(b"ESPB");
const MTF_ESET: u32 = str2u32!(b"ESET");
const MTF_EOTM: u32 = str2u32!(b"EOTM");
const MTF_SFMB: u32 = str2u32!(b"SFMB");

#[derive(Debug, Clone, Copy, PartialEq, Eq, TryFromPrimitive, IntoPrimitive)]
#[repr(u32)]
pub enum BlockType {
    TapeHeader = MTF_TAPE,
    StartOfDataSet = MTF_SSET,
    Volume = MTF_VOLB,
    Directory = MTF_DIRB,
    File = MTF_FILE,
    CorruptObject = MTF_CFIL,
    EndOfSetPad = MTF_ESPB,
    EndOfSet = MTF_ESET,
    EndOfTapeMarker = MTF_EOTM,
    SoftFileMark = MTF_SFMB,
}

#[repr(u8)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
pub enum StringType {
    NoStrings = 0,
    AnsiStrings = 1,
    UnicodeStrings = 2,
}

impl StringType {
    pub fn parse_string(self, buf: &[u8]) -> String {
        match self {
            StringType::UnicodeStrings => UTF_16LE.decode_without_bom_handling(buf).0.to_string(),
            StringType::AnsiStrings => WINDOWS_1252.decode_without_bom_handling(buf).0.to_string(),
            _ => panic!(),
        }
    }
}

/*bitflags! {
    pub struct MTFBlockAttributes: u32 {
        const MTF_CONTINUATION = 1 << 0;
        const MTF_COMPRESSION = 1 << 2;
        const MTF_EOS_AT_EOM = 1 << 3;
        const MTF_SET_MAP_EXISTS = 1 << 16;
        const MTF_FDD_ALLOWED = 1 << 17;
        const MTF_FDD_EXISTS = 1 << 16;
        const MTF_ENCRYPTION = 1 << 17;
        const MTF_FDD_ABORTED = 1 << 16;
        const MTF_END_OF_FAMILY = 1 << 17;
        const MTF_ABORTED_SET = 1 << 18;
        const MTF_NO_ESET_PBA = 1 << 16;
        const MTF_INVALID_ESET_PBA = 1 << 17;
    }
}*/

pub const MTF_CONTINUATION: u32 = 1;
pub const MTF_COMPRESSION: u32 = 1 << 2;
pub const MTF_EOS_AT_EOM: u32 = 1 << 3;
pub const MTF_SET_MAP_EXISTS: u32 = 1 << 16;
pub const MTF_FDD_ALLOWED: u32 = 1 << 17;
pub const MTF_FDD_EXISTS: u32 = 1 << 16;
pub const MTF_ENCRYPTION: u32 = 1 << 17;
pub const MTF_FDD_ABORTED: u32 = 1 << 16;
pub const MTF_END_OF_FAMILY: u32 = 1 << 17;
pub const MTF_ABORTED_SET: u32 = 1 << 18;
pub const MTF_NO_ESET_PBA: u32 = 1 << 16;
pub const MTF_INVALID_ESET_PBA: u32 = 1 << 17;

#[derive(Debug, Copy, Clone)]
pub struct TapeHeaderData {
    pub media_family_id: u32,
    pub tape_attributes: u32,
    pub media_sequence_number: u16,
    pub password_encryption_algorithm: u16,
    pub soft_filemark_block_size: u16,
    pub media_based_catalog_type: u16,
    pub media_name: TapeAddress,
    pub media_description_label: TapeAddress,
    pub media_password: TapeAddress,
    pub software_name: TapeAddress,
    pub format_logical_block_size: u16,
    pub software_vendor_id: u16,
    pub media_date: DateTime,
    pub mtf_major_version: u8,
}

#[derive(Debug)]
pub struct MediaLabel {
    pub tag: String,
    pub version: String,
    pub vendor: String,
    pub vendor_product_id: String,
    pub creation_time_stamp: String,
    pub cartridge_label: String,
    pub side: u8,
    pub media_id: String,
    pub media_domain_id: String,
    // vendor_specific: Vec<String>
}

impl MediaLabel {
    pub fn from_string(label: &str) -> Self {
        let mut label_values = label.split('|');
        let tag = label_values.next().unwrap().to_owned();
        let version = label_values.next().unwrap().to_owned();
        let vendor = label_values.next().unwrap().to_owned();
        let vendor_product_id = label_values.next().unwrap().to_owned();
        let creation_time_stamp = label_values.next().unwrap().to_owned();
        let cartridge_label = label_values.next().unwrap().to_owned();
        let side = label_values.next().unwrap().to_owned();
        let media_id = label_values.next().unwrap().to_owned();
        let media_domain_id = label_values.next().unwrap().to_owned();
        Self {
            tag,
            version,
            vendor,
            vendor_product_id,
            creation_time_stamp,
            cartridge_label,
            side: side.parse().unwrap_or(1),
            media_id,
            media_domain_id,
        }
    }
}

impl TapeHeaderData {
    pub fn read<T>(read: &mut T) -> io::Result<Self>
    where
        T: Read,
    {
        let media_family_id = read.read_u32()?;
        let tape_attributes = read.read_u32()?;
        let media_sequence_number = read.read_u16()?;
        let password_encryption_algorithm = read.read_u16()?;
        let soft_filemark_block_size = read.read_u16()?;
        let media_based_catalog_type = read.read_u16()?;
        let media_name = TapeAddress::read(read)?;
        let media_description_label = TapeAddress::read(read)?;
        let media_password = TapeAddress::read(read)?;
        let software_name = TapeAddress::read(read)?;
        let format_logical_block_size = read.read_u16()?;
        let software_vendor_id = read.read_u16()?;
        let media_date = DateTime::read(read)?;
        let mtf_major_version = read.read_u8()?;
        Ok(Self {
            media_family_id,
            tape_attributes,
            media_sequence_number,
            password_encryption_algorithm,
            soft_filemark_block_size,
            media_based_catalog_type,
            media_name,
            media_description_label,
            media_password,
            software_name,
            format_logical_block_size,
            software_vendor_id,
            media_date,
            mtf_major_version,
        })
    }
}

#[derive(Debug, Copy, Clone)]
pub struct CommonBlockHeader {
    pub descriptor_block_type: BlockType,
    pub block_attributes: u32,
    pub offset_to_first_event: u16,
    pub os_id: u8,
    pub os_version: u8,
    pub displayable_size: u64,
    pub format_logical_address: u64,
    pub reserved_for_mbc: u16,
    pub control_block_id: u32,
    pub os_specific_data: TapeAddress,
    pub string_type: StringType,
    pub header_checksum: u16,
}

impl CommonBlockHeader {
    pub fn read<T>(read: &mut T) -> io::Result<Self>
    where
        T: Read,
    {
        let descriptor_block_type = read.read_u32()?;
        let block_attributes = read.read_u32()?;
        let offset_to_first_event = read.read_u16()?;
        let os_id = read.read_u8()?;
        let os_version = read.read_u8()?;
        let displayable_size = read.read_u64()?;
        let format_logical_address = read.read_u64()?;
        let reserved_for_mbc = read.read_u16()?;
        io::copy(&mut read.by_ref().take(6), &mut io::sink())?;
        let control_block_id = read.read_u32()?;
        io::copy(&mut read.by_ref().take(4), &mut io::sink())?;
        let os_specific_data = TapeAddress::read(read)?;
        let string_type: StringType = read.read_u8()?.try_into().unwrap();
        io::copy(&mut read.by_ref().take(1), &mut io::sink())?;
        let header_checksum = read.read_u16()?;
        Ok(Self {
            descriptor_block_type: BlockType::try_from(descriptor_block_type).unwrap(),
            block_attributes,
            offset_to_first_event,
            os_id,
            os_version,
            displayable_size,
            format_logical_address,
            reserved_for_mbc,
            control_block_id,
            os_specific_data,
            string_type,
            header_checksum,
        })
    }
}

#[derive(Debug, Clone)]
pub struct SoftFilemarkData {
    pub filemark_entries: u32,
    pub filemark_entries_used: u32,
    pub previous_filemark_pbas: Vec<u32>,
}

impl SoftFilemarkData {
    pub fn read<T>(read: &mut T) -> io::Result<Self>
    where
        T: Read,
    {
        let filemark_entries = read.read_u32()?;
        let filemark_entries_used = read.read_u32()?;
        let mut previous_filemark_pbas = Vec::<u32>::with_capacity(filemark_entries as usize);
        for _ in 0..filemark_entries_used {
            previous_filemark_pbas.push(read.read_u32()?);
        }
        Ok(SoftFilemarkData {
            filemark_entries,
            filemark_entries_used,
            previous_filemark_pbas,
        })
    }
}

#[derive(Debug, Copy, Clone)]
pub struct StartOfDataSetData {
    pub attributes: u32,
    pub password_encryption_algorithm: u16,
    pub software_compression_algorithm: u16,
    pub software_vendor_id: u16,
    pub data_set_number: u16,
    pub data_set_name: TapeAddress,
    pub data_set_description: TapeAddress,
    pub data_set_password: TapeAddress,
    pub user_name: TapeAddress,
    pub physical_block_address: u64,
    pub media_write_date: DateTime,
    pub software_major_version: u8,
    pub software_minor_version: u8,
    pub time_zone: i8,
    pub mtf_minor_version: u8,
    pub media_catalog_version: u8,
}

impl StartOfDataSetData {
    pub fn read<T>(read: &mut T) -> io::Result<Self>
    where
        T: Read,
    {
        let attributes = read.read_u32()?;
        let password_encryption_algorithm = read.read_u16()?;
        let software_compression_algorithm = read.read_u16()?;
        let software_vendor_id = read.read_u16()?;
        let data_set_number = read.read_u16()?;
        let data_set_name = TapeAddress::read(read)?;
        let data_set_description = TapeAddress::read(read)?;
        let data_set_password = TapeAddress::read(read)?;
        let user_name = TapeAddress::read(read)?;
        let physical_block_address = read.read_u64()?;
        let media_write_date = DateTime::read(read)?;
        let software_major_version = read.read_u8()?;
        let software_minor_version = read.read_u8()?;
        let time_zone = read.read_i8()?;
        let mtf_minor_version = read.read_u8()?;
        let media_catalog_version = read.read_u8()?;
        Ok(Self {
            attributes,
            password_encryption_algorithm,
            software_compression_algorithm,
            software_vendor_id,
            data_set_number,
            data_set_name,
            data_set_description,
            data_set_password,
            user_name,
            physical_block_address,
            media_write_date,
            software_major_version,
            software_minor_version,
            time_zone,
            mtf_minor_version,
            media_catalog_version,
        })
    }
}

#[derive(Debug, Clone, Copy)]
pub struct VolumeData {
    pub attributes: u32,
    pub device_name: TapeAddress,
    pub volume_name: TapeAddress,
    pub machine_name: TapeAddress,
    pub media_write_date: DateTime,
}

impl VolumeData {
    pub fn read<T>(read: &mut T) -> io::Result<Self>
    where
        T: Read,
    {
        let attributes = read.read_u32()?;
        let device_name = TapeAddress::read(read)?;
        let volume_name = TapeAddress::read(read)?;
        let machine_name = TapeAddress::read(read)?;
        let media_write_date = DateTime::read(read)?;
        Ok(Self {
            attributes,
            device_name,
            volume_name,
            machine_name,
            media_write_date,
        })
    }
}

#[derive(Debug, Copy, Clone)]
pub struct DirectoryData {
    pub attributes: u32,
    pub last_modification_date: DateTime,
    pub creation_date: DateTime,
    pub backup_date: DateTime,
    pub last_access_time: DateTime,
    pub directory_id: u32,
    pub directory_name: TapeAddress,
}

impl DirectoryData {
    pub fn read<T>(read: &mut T) -> io::Result<Self>
    where
        T: Read,
    {
        let attributes = read.read_u32()?;
        let last_modification_date = DateTime::read(read)?;
        let creation_date = DateTime::read(read)?;
        let backup_date = DateTime::read(read)?;
        let last_access_time = DateTime::read(read)?;
        let directory_id = read.read_u32()?;
        let directory_name = TapeAddress::read(read)?;
        Ok(Self {
            attributes,
            last_modification_date,
            creation_date,
            backup_date,
            last_access_time,
            directory_id,
            directory_name,
        })
    }
}

#[derive(Debug, Copy, Clone)]
pub struct FileData {
    pub attributes: u32,
    pub last_modification_date: DateTime,
    pub creation_date: DateTime,
    pub backup_date: DateTime,
    pub last_access_date: DateTime,
    pub directory_id: u32,
    pub file_id: u32,
    pub file_name: TapeAddress,
}

impl FileData {
    pub fn read<T>(read: &mut T) -> io::Result<Self>
    where
        T: Read,
    {
        let attributes = read.read_u32()?;
        let last_modification_date = DateTime::read(read)?;
        let creation_date = DateTime::read(read)?;
        let backup_date = DateTime::read(read)?;
        let last_access_date = DateTime::read(read)?;
        let directory_id = read.read_u32()?;
        let file_id = read.read_u32()?;
        let file_name = TapeAddress::read(read)?;
        Ok(Self {
            attributes,
            last_modification_date,
            creation_date,
            backup_date,
            last_access_date,
            directory_id,
            file_id,
            file_name,
        })
    }
}

#[derive(Debug, Copy, Clone)]
pub struct CorruptObjectData {
    pub attributes: u32,
    pub stream_offset: u64,
    pub corrupt_stream_number: u16,
}

impl CorruptObjectData {
    pub fn read<T>(read: &mut T) -> io::Result<Self>
    where
        T: Read,
    {
        let attributes = read.read_u32()?;
        io::copy(&mut read.by_ref().take(8), &mut io::sink())?;
        let stream_offset = read.read_u64()?;
        let corrupt_stream_number = read.read_u16()?;
        Ok(Self {
            attributes,
            stream_offset,
            corrupt_stream_number,
        })
    }
}

#[derive(Debug, Copy, Clone)]
pub struct EndOfSetData {
    attributes: u32,
    corrupt_files: u32,
    fdd_media_sequence_number: u16,
    data_set_number: u16,
    media_write_date: DateTime,
}

impl EndOfSetData {
    pub fn read<T>(read: &mut T) -> io::Result<Self>
    where
        T: Read,
    {
        let attributes = read.read_u32()?;
        let corrupt_files = read.read_u32()?;
        io::copy(&mut read.by_ref().take(16), &mut io::sink())?;
        let fdd_media_sequence_number = read.read_u16()?;
        let data_set_number = read.read_u16()?;
        let media_write_date = DateTime::read(read)?;
        Ok(Self {
            attributes,
            corrupt_files,
            fdd_media_sequence_number,
            data_set_number,
            media_write_date,
        })
    }
}

#[derive(Debug, Copy, Clone)]
pub struct EndOfTapeMarkerData {
    pub last_eset_pba: u64,
}

impl EndOfTapeMarkerData {
    pub fn read<T>(read: &mut T) -> io::Result<Self>
    where
        T: Read,
    {
        let last_eset_pba = read.read_u64()?;
        Ok(Self { last_eset_pba })
    }
}

#[derive(Debug, Clone)]
pub enum DescriptorBlockData {
    TapeHeader(TapeHeaderData),
    StartOfDataSet(StartOfDataSetData),
    Volume(VolumeData),
    Directory(DirectoryData),
    File(FileData),
    CorruptObject(CorruptObjectData),
    EndOfSetPad,
    EndOfSet(EndOfSetData),
    EndOfTapeMarker(EndOfTapeMarkerData),
    SoftFileMark(SoftFilemarkData),
}

#[derive(Debug)]
pub struct DescriptorBlock {
    pub base_address: u64,
    pub common_block_header: CommonBlockHeader,
    pub block_data: DescriptorBlockData,
}

pub fn read_address<T>(
    stream: &mut T,
    base_offset: u64,
    address: TapeAddress,
) -> io::Result<Vec<u8>>
where
    T: Read + Seek,
{
    let offset: u64 = address.offset.into();
    let size: usize = address.size.into();

    let current = stream.seek(SeekFrom::Current(0))?;
    stream.seek(SeekFrom::Start(base_offset + offset))?;

    let mut buf = vec![0u8; size];
    stream.read_exact(&mut buf)?;

    stream.seek(SeekFrom::Start(current))?;
    Ok(buf)
}

impl DescriptorBlock {
    pub fn read<T>(read: &mut T) -> io::Result<Self>
    where
        T: Read + Seek,
    {
        let base_address = read.seek(SeekFrom::Current(0))?;
        let common_block_header = CommonBlockHeader::read(read)?;
        let block_data = match common_block_header.descriptor_block_type {
            BlockType::TapeHeader => DescriptorBlockData::TapeHeader(TapeHeaderData::read(read)?),
            BlockType::StartOfDataSet => {
                DescriptorBlockData::StartOfDataSet(StartOfDataSetData::read(read)?)
            }
            BlockType::Volume => DescriptorBlockData::Volume(VolumeData::read(read)?),
            BlockType::Directory => DescriptorBlockData::Directory(DirectoryData::read(read)?),
            BlockType::File => DescriptorBlockData::File(FileData::read(read)?),
            BlockType::CorruptObject => {
                DescriptorBlockData::CorruptObject(CorruptObjectData::read(read)?)
            }
            BlockType::EndOfSetPad => DescriptorBlockData::EndOfSetPad,
            BlockType::EndOfSet => DescriptorBlockData::EndOfSet(EndOfSetData::read(read)?),
            BlockType::EndOfTapeMarker => {
                DescriptorBlockData::EndOfTapeMarker(EndOfTapeMarkerData::read(read)?)
            }
            BlockType::SoftFileMark => {
                DescriptorBlockData::SoftFileMark(SoftFilemarkData::read(read)?)
            }
        };
        Ok(Self {
            base_address,
            common_block_header,
            block_data,
        })
    }

    pub fn read_string_at<T>(
        &self,
        stream: &mut T,
        address: TapeAddress,
    ) -> Option<io::Result<String>>
    where
        T: Read + Seek,
    {
        if address.is_present() {
            Some(
                read_address(stream, self.base_address, address)
                    .map(|data| self.common_block_header.string_type.parse_string(&data)),
            )
        } else {
            None
        }
    }
}
