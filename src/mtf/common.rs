use super::read::MtfReadExt;
use std::io::{Read, Result};

#[macro_export]
macro_rules! str2u32 {
    ($e:expr) => (u32::from_le_bytes(*$e));
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct TapeAddress {
    pub size: u16,
    pub offset: u16,
}

impl TapeAddress {
    pub fn read<T>(read: &mut T) -> Result<Self>
    where
        T: Read,
    {
        let size = read.read_u16()?;
        let offset = read.read_u16()?;
        Ok(TapeAddress { size, offset })
    }

    pub fn is_present(self) -> bool {
        self.size > 0 && self.offset > 0
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct DateTime {
    year: u16,
    month: u8,
    day: u8,
    hour: u8,
    minute: u8,
    second: u8,
}

impl DateTime {
    pub fn read<T>(read: &mut T) -> Result<Self>
    where
        T: Read,
    {
        let mut array = [0u8; 5];
        read.read_exact(&mut array)?;
        let year: u16 = ((array[0] as u16) << 6) | (((array[1] & 0xFC) >> 2) as u16);
        let month: u8 = ((array[1] & 0x03) << 2) | ((array[2] & 0xC0) >> 6);
        let day: u8 = (array[2] & 0x3E) >> 1;
        let hour: u8 = ((array[2] & 0x01) << 4) | ((array[3] & 0xF0) >> 4);
        let minute: u8 = ((array[3] & 0x0F) << 2) | ((array[4] & 0xC0) >> 6);
        let second: u8 = array[4] & 0x3F;
        Ok(DateTime {
            year,
            month,
            day,
            hour,
            minute,
            second,
        })
    }
}
